.. _rc2_toolrepo_requirements:

Tool Registry Requirements
==========================

Specific Functionality of the GFOI Tools Registry
-------------------------------------------------

The Tool Registry will allow a user from a country to:
  - find a tool using a search which fits their country context,
  - explorer cross cutting and related concepts to the use of the tool
  - answer the question of how the tool would fit with other concepts
  - get an understanding of the NFMS parts that are supported by the tool

The assumed knowledge of the user is:
`````````````````````````````````````
  - Knows the country context and existing tools, design, data, and
    requirements
  - Can use complex software and has some technical user skills
  - Speaks and reads one of the following languages; English, Spanish, or French
  - Has some technical system administration knowledge or can either find out
    independently from other sources
  - Can get help or allocate the work involving system administration to
    someone else
  - Does not know specific tool information and configuration
  - Understands REDD+, NFMS, MRV concepts


NFMS/MRV activities use centric view:
`````````````````````````````````````

.. hlist::
  :columns: 2

  - **Wireframe: Search** |RoT-Wireframe-Search|
  - **Wireframe: Criteria Compare** |RoT-Wireframe-Criteria|


- The tools are listed and searched based on the data input and output.
- Also concepts deal with the objective use case of the tool in the NFMS/MRV
  activities - Task filter (i.e. Inventory)
- Used with activities related to spacial aspects filter
- Satellite data tools filter
- Integration activities filter

.. |RoT-Wireframe-Search| thumbnail:: figures/RoT-Wireframes-Search.png
  :width: 100px

.. |RoT-Wireframe-Criteria| thumbnail:: figures/RoT-Wireframes-Criteria.png
  :width: 100px

Tool Suite view:
````````````````
  - A suite of tools is a set of tools which work together from end to end of
    the NFMS/MRV activities.  - Everything needed to get the tools to work
    (configuration, inter operation scripts, ....) is induced in a suite
  - There are 3 ‘levels’ of sophistication Suite A, B, and C
  - Countries need a path from what they have now and things they could change
    ‘modules’ - The suites can be as generic as possible with clear
    instructions
  - The possible tailoring and optional aspects of each suite are documented
  - Some sort of graphical representation of the suites, options, parts ,....
  - Improvements or alternatives are included within the suites.
  - This could be based on general profiles of a survey of country’s current
    NFMS/MRV’s

NFMS/MRV System Centric View:
`````````````````````````````
.. todo:: CNA diagram images with linking and selection based on context in the system.


Tool Profile View:
``````````````````

**Wireframe: Tool Profile**
  |RoT-Wireframe-ToolProfile|

The tool profile view will display all the information available for a tool.

  - This information can be displayed as a standalone page
  - The view will work as a part of an expanded view

.. |RoT-Wireframe-ToolProfile| thumbnail:: figures/RoT-Wireframes-ToolProfile.png
  :width: 100px

High level functional design and architecture of the ToolRegistry
-----------------------------------------------------------------
**Hosting**
    - The user interface files (Front End) will be hosted on the REDDcompass
      systems
    - The SOP files will be hosted on the REDDcompass systems
    - The recipes will be hosted on the REDDcompass systems
    - The demonstration PDF and pictures will be hosted on the REDDcompass
      systems
    - The demonstration Videos maybe hosted on REDDcompass or externally hosted
      on a platform such as Vimeo, YouTube,…
    - Display and playback of externally hosted Video will be embedded in the
      Front End system
    - Some space will be allocated for hosting small tools on the REDDcompass
      system
    - The ToolRegistry will link to externally hosted tools
**Knowledge**
    - Production of the demonstration PDF, Pictures, and Video will be outside
      the development of the ToolRegistry.
    - The ToolRegistry will allow the upload/integration of the demonstration
      materials on a ongoing basis
    - The ToolRegistry will allow the management and update of the ‘Knowledge’
      contained in the system
**Front End**
    - The Front End will be light enough to work in countries with poor
      internet connections
    - The ToolRegistry will support users being able to download the tools
      outside of the Front End (You can get the raw link and cut and paste it
      somewhere else)
    - The Look and Feel is not restricted by the Look and Feel of REDDcompass

Requirements:Tools Registry (ToolRegistry)
------------------------------------------

.. req:: The search for a tool will filter the results for a search based on
  criteria from the user.

.. req:: The user will be able to ‘select’ and define there context
.. req:: The search for a tool will filter based on a users ‘selected’ context
.. req:: The search for a tool will filter based on the tool suite selected
.. req:: The search for a tool will filter based on the NFMS/MRV activity use
  selected
.. req:: The search for a tool will filter based on the NFMS/MRV system part

.. req:: A recipe will be the steps required to adopt a tool.
.. req:: A recipe will contain all the information on where to get software and
  the details of the software
.. req:: A recipe will contain all the system configuration, settings, scripts
  required
.. req:: A recipe will contain all the instructions to install the tool from an
  assumed starting point
.. req:: A recipe will contain all the instructions to make the tool completely
  operational
.. req:: A recipe will have a description of ‘what can go wrong’, ‘how to
  check’, and ‘what to do about it’
.. req:: A recipe will have demonstrations captured in PDF, Video, Pictures of
  some the steps involved
.. req:: A recipe may have links to other material
.. req:: A standard operating procedure (SOP) will have the correct work flow
  for using the tool

.. req:: A SOP will have the expected output, checks, and validation techniques
  for assessing that the tool has been used correctly
.. req:: A SOP will detail the constraints on the expected input of the tool
.. req:: A SOP will list the record keeping, quality control information,
  verification evidence involved
.. req:: A SOP will list the catalogue method, naming, versioning criteria and
  other methods required to be used with the output and input
.. req:: The SOP will have demonstrations captured in PDF, Video, Pictures of
  some the tasks involved in the workflow
.. req:: A SOP may have links to other material
