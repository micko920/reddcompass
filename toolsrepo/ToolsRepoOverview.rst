.. _rc2_toolrepo_overview:

GFOI Tools Registry
===================

There is a broad and dispersed range of tools countries may use to Measure,
Report and Verify greenhouse gas emissions from forests. Currently countries
may not be aware of all the options available.

The GFOI Tools Registry will facilitate the dynamic connecting of information
about tools to the GFOI Methods and Guidance Documentation. This new connection
will support countries to find the tools that fit within their operational
environment.

The survey of available tools will also identify country needs which are not
currently supported by the available tools. GFOI aims to champion the adoption
and development of new tools to fill these gaps. The R&D can then help to
progress tools towards an operational phase. The capacity building and
implementation partners will be able to use the GFOI Tools Registry as a
reference for up to date information.

The GFOI Tool Registry will be an online platform for communicating the
available tools based on categories and search criteria. It aims to help
organize the tools and assist countries in making informed decisions on which
tool(s) meets their needs.

The tool information, MGD sections and the REDDcompass Actions will be linked
and cross referenced to allow the user to explore between the different
concepts and resources.

What tools are included
-----------------------

For the purpose of this registry, the definition of a tool is any piece of
technology that directly or indirectly supports National Forest Monitoring
Systems (NFMS) and associated measurement, reporting and verification (MRV)
procedures.  This includes technology which can be used directly within an NFMS
or is used to prepare any inputs or outputs. The GFOI registry of tools may
include software packages that have a focus on broader applications, however
the software should be capable of addressing specific REDDcompass actions.

Prototype of the functionality
------------------------------

A prototype of the tool is explained in :doc:`ToolsRepoPrototype`.


Context
  To date, only a Prototype of the GFOI Tools Registry has been developed.
  This document has been prepared to guide partners through the prototype.

  The prototype has been developed to propose options for functionality and the
  types of information which can potentially be displayed on the Registry. We
  are now seeking comments/input on this proposal from the Data Component
  co-Leads and other GFOI partners who have been involved in the process, to
  help inform the next phase of development.

Appearance
  The prototype styling is different to how the final product will look.  It is
  proposed that the interface for the GFOI Registry of Tools will have a very
  similar look and feel to REDDcompass which may also change as a result of the
  current upgrade. The exact appearance of the Registry is still TBC, however
  it will be based on Google’s standard material design guidelines.

Functionality
  The home page will feature a default list of tools submitted by partners to
  date, which have been confirmed as operational (according to CALM) and relevant
  to a REDDcompass action or section of the MGD (as confirmed by the MGD AG).
  Clicking on any of the listed tools will display a sub page of relevant
  information on that tool – see the Tool Information View tab in the prototype
  for the proposed information to be displayed. In summary, this includes an
  Overview description of the tool, a panel on its MRV Technical Details and
  another panel of General Information (including links to tools, manuals and
  contacts, etc).

  The list on the homepage will be able to be filtered according to
  predetermined criteria, such as pre-operational tools (i.e. R&D phase),
  remote sensing and/or ground data relevant, language availability (i.e.
  English, Spanish and French) and various other criteria. These criteria are
  still to be confirmed and suggestions are most welcome.

  The operational tools will also be displayed with the relevant actions within
  REDDcompass and the relevant sections within the MGD 3.0. Clicking on these
  links will take you to the same subpages (Tool Information View) mentioned
  above.
