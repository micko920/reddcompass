.. _rc2_toolrepo_prototype:

Tools Registry Prototype
========================

This is the documentation and guide for the Tools Registry Prototype. It
explains the interface and features.

The Tools catalogue functionality of REDDcompass can be found at
:doc:`ToolsRepoOverview`. An explanation of the fields in the Tools Registry
can be found here in :doc:`ToolsRepoDataFields`.

Main link for the prototype
---------------------------
The main link for the prototype is: https://micko920.gitlab.io/rc_table

The purpose of this prototype is to test layout ideas and information display.
The menu and tool bars help show each control. They are not part of REDDcompass
2 or the tool registry.

|RoT-Prototype-Menu|

The picture above shows the menu list on the left of the tool prototype screen.
This menu lists the available 'modes' of viewing the Tools.  Selecting the
'Tool Information View' will display what a Tool Information Profile would look
like.  In this case the information is not a real tool.

.. |RoT-Prototype-Menu| thumbnail:: figures/toolPrototypeMenu.png
  :width: 180px

Tool Information View
---------------------
|RoT-Prototype-InfoView|
  Link: `Prototype Tool Information View`_

The best place to start is the tool information view. This is how
all the information would be displayed if a user clicks on a link of a "tool".

Clicking on the text **"Some great tool"** will display an expanded information
panel. All information is grouped into sections and displayed for that
tool.

|RoT-Prototype-InfoViewBreakdown|
The image above shows the expanded view of tool information. The name **"Some
great tool"** and logo are a link to the tool website. The flag is the main
language of the tool and the CALM assessment is included on the right.

.. |RoT-Prototype-InfoView| thumbnail:: figures/toolInfoView.png
  :width: 100%

.. _Prototype Tool Information View: https://micko920.gitlab.io/rc_table/infoView

.. |RoT-Prototype-InfoHeader| thumbnail:: figures/toolInfoViewHeader.png
  :width: 100%

.. |RoT-Prototype-InfoViewBreakdown| thumbnail:: figures/RoT-ToolInfoViewBreakdown.png
  :width: 100%


Content Extension Tool List View
--------------------------------
The list version of the interface can be found at the following link. This is
to demonstrate all the tools as a list with a limited number of columns.
This works quite well with the search box at the top to find a tool for an MRV
objective or task.

https://micko920.gitlab.io/rc_table/contentExtensions

|RoT-Prototype-ListView|
In the image above you can see the list of tools. At the top of the list is a
search box and column headings. Typing text in the search box will filter the
list based on the visible column text. Clicking the headers will change the
sort order of the column.


|RoT-Prototype-ListViewExpand| At the left of each row is a
:awesome:`fa-chevron-down`. Clicking on this icon will expand the information
panel for that tool.

|
|
|

.. |RoT-Prototype-ListViewExpand| thumbnail:: figures/toolListViewExpand.png
  :width: 400px
  :align: left

.. |RoT-Prototype-ListView| thumbnail:: figures/toolListView.png
  :width: 100%



Simple Table View
-----------------
This is all the data in a 'simple' table which is included this to demonstrate
that this may not the best interface for the information.

https://micko920.gitlab.io/rc_table/simpleTable


Tool data included in the Prototype
-----------------------------------
All the data in this is not "live". It is based on a version of the spread
sheet collected from the google form.  It has not been cleaned or corrected.
The logos, flags, and some data has been added to allow examples to work.

