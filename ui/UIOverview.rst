.. _rc2_ui_overview:

User interface Overview
=======================

|UIOverview|

The User Interface (UI) is the conjunction of different Information views. The
user can then move from one information view with a particular focus to an
other based on their current context.

Each information view has a particular focus and structure. For example the MGD
Content prioritises the MGD chapter, section and contents order. The
MGD Knowledge is organised into Theme and concepts which is represented by
colour and triangle shapes.

.. |UIOverview| thumbnail:: figures/AllInformationModelsOverview.svg
    :width: 400px
    :align: center


Main User Storyline (Use Case)
------------------------------
|UIStoryline|

The image above depicts the REDDcompass user context. The main aim of a user is
to improve the NFMS operations.  REDDcompass helps to do this by supporting the
user to locate and understand the specific information related to the desired
NFMS improvements.

Story Line: Product Gap
  i.e. Degradation Emissions or improvements to identification of areas subject
  to degradation

  **Notes**: Spatially Referenced Data


Story line: Nationally Specific Emission Factors Gap
  i.e. Forest to Crop land conversion Emission Factors

  **Notes**:


Story line: New Outcome or Purpose for NFMS
  i.e. Submit National FRL to UNFCCC

  **Notes**:

.. |UIStoryline| thumbnail:: figures/StoryLine.svg
    :width: 400px
    :align: center


Assumptions about the user
--------------------------

During the development of the REDDcompass system it helped to outline details
about the user and the context of the use of the system.

- The country is the primary focus of the story line
- The gap in the current NFMS is already identified
- The aim is to improving the operations of an exiting NFMS
- The country is the main actor progressing the story line activities

This does not mean that other users or contexts will not be supported. It only
provides a focus for the design decisions and the imagination.



