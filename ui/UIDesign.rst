.. _rc2_ui_design:

User interface Design
=====================


User Interface inspiration examples
-----------------------------------

.. todo:: list and discus user interface examples used for inspiration
.. todo:: add reference for different tool registry examples
.. todo:: add reference for Google analytics slide in extended views `draws`


Main interface control frame
----------------------------
.. todo:: add main application frame detail

Pop up and extended information view
------------------------------------
.. todo:: add draws, popup, hover bubble detail

Visual Terminology and Nomenclature
-----------------------------------
.. todo:: add icon definitions

Specialist Controls
-------------------
.. todo:: Pyramid


Try to embed:: micko920.gitlab.io/rc-stylegallery/pyramid


.. todo:: CNA NFMS system view and editing
.. todo:: Decision Trees
.. todo:: CNA card and data recording


