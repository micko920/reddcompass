.. _rc2_cna_overview:

GFOI Country Needs Assessment Workshop Brief
============================================

What is the GFOI Country Needs Assessment Workshop?
---------------------------------------------------

The GFOI Country Needs Assessment (CNA) workshop facilitates a systematic
self-evaluation, by a REDD+ country, to identify current needs to
operationalise and mature their national forest monitoring systems (NFMS) and
associated emissions measurement, reporting and verification (MRV)
requirements. Once identified, these needs can then be clearly communicated
both internally and to development partners to target support at addressing
these needs.

The GFOI CNA workshop steps countries through i) Representation of their
existing NFMS; ii) Assessment of its ’in use’ performance iii) Identification
of needs; iv) Identification of work packages to address those needs, and v)
Presentation of findings. The analysis is aimed at leveraging existing progress
in designing and implementing the NFMS and helping to focus on what needs to be
done next to operationalise and mature the end-to-end system.

The GFOI CNA workshop is system centric, using cards as visual cues to
represent the NFMS. The systems analysis is conducted in an unconstrained group
environment to gather detail of the current existing NFMS (i.e. system ‘as
is’), represented by the data, people, processes, documentation and reports.

The GFOI CNA workshop activities produce a technical representation of the
system in a series of tables, graphs and images using established systems
analysis techniques.

The self-review and assessment of ‘in-use’ performance of the NFMS is framed
around REDDcompass Actions which have been categorised based on sophistication,
focus, and system disciplines. Different categories are used depending on the
current NFMS maturity. The GFOI CNA workshop activities can be repeated to show
progressive improvement of the NFMS maturity.

How does this workshop and its outputs relate to GFOI?
------------------------------------------------------

The GFOI CNA workshop activities are based on MGD guidance and REDDcompass and
supplement GFOI partner programs. It does not replace existing programs and
relationships nor is it an audit. It is an open, constructive and collaborative
exploration of needs. The GFOI CNA workshop:

  - identifies needs within the existing country’s context and national goals
  - focuses on assessing and improving the tangible elements of a country’s
    NFMS (i.e. data, processes and documents)
  - identifies work packages that progress and mature the operation of their NFMS.

.. sidebar:: Example: How a country’s World Bank FCPF R-Package contributes to the GFOI CNA Workshop

  The Readiness package (or R-Package) is a key milestone in the pathway for
  countries to progress from REDD+ Readiness to commence implementation (i.e.
  the Carbon Fund). The self-assessment criteria of the FCPF Readiness
  Assessment Framework enable countries to assess whether the following four
  components are capable of existing (i.e. is the country context ready for
  REDD+): 1) Readiness Organization and Consultation; 2) REDD+ Strategy
  Preparation; 3) Reference level 4) Monitoring Systems (a. Forests, b.
  Safeguards).  The GFOI CNA workshop considers the operational aspects of the
  NFMS (i.e. R-Package Component 4a). The GFOI CNA workshop activities are
  informed by and utilize the context of the R-Package (and other existing
  documents), enabling a country to assess progress against country identified
  goals for the NFMS.

What are the roles involved in the GFOI CNA workshops?
------------------------------------------------------

Country participants
  the country participants should have technical skills from a range of NFMS
  thematic areas and the responsibility or motivation to champion the
  improvements that will operationalise the NFMS in their country.

Facilitators
  Using the GFOI CNA Workshop Facilitator handbook, the facilitator guides
  workshop participants through a series of activity worksheets. Each worksheet
  contributes to the workshop outputs.

Coaches
  Coaches use their country specific knowledge, and technical experience of
  NFMS, to ask questions to stimulate insight into the country specific NFMS
  needs.

Support Funding Agency
  The GFOI CNA workshops are funded by a GFOI partner and may be in regional or
  single country formats. These agencies fund the workshops to primarily help
  countries but to also better understand how they can best target their
  assistance at countries needs to operationalise their NFMS.



What are the GFOI CNA workshop outputs and how can they be used?
----------------------------------------------------------------
The GFOI CNA activities produce a number of country specific outputs listed in Table 1.

.. table:: Table 1: GFOI CNA Workshop Outputs

 +---------------------------------+------------------------------------------+
 | Output Content                  | Description                              |
 +=================================+==========================================+
 | Country Summary Report          | * Technical representation of the system |
 |                                 | * System Assessment                      |
 |                                 | * System Objectives                      |
 +---------------------------------+------------------------------------------+
 | Improvement Plan                | * Work Packages                          |
 |                                 | * Planning                               |
 +---------------------------------+------------------------------------------+
 | Country Powerpoint Presentation | Communication of workshop insights       |
 |                                 | and outputs                              |
 +---------------------------------+------------------------------------------+

.. sidebar:: Example: Working together for country ownership and leadership.

  Development partners can serve as effective coaches for country participants,
  working together post workshop to co-design (through defining work packages),
  co-implement (through supporting project management) and co-monitor (through
  repeating the CNA processes) the identified needs from the initial CNA
  workshop. The workshop outcomes can also inform refinements to development
  partner existing work plans to address identified needs. Additionally,
  Representatives from development partners may have skills and experience in
  particular thematic areas (e.g. National Forest Inventories, Satellite Land
  Monitoring Systems and Web Portal Development) bringing some technical
  support to fulfilling operational needs.

The information captured during the GFOI CNA workshop should be taken home by
the country participants to facilitate further discussion and co-ordination on
the implementation of NFMS improvements. The information can also be useful in
communicating system design and operation to external stakeholders.

Additionally the information is collected by GFOI and made available to its
development partners to inform their work planning and ultimately help target
their support at country-identified needs to operationalise their NFMS. GFOI
encourages countries’ to use the CNA outputs to communicate progress and
refinement of identified needs in a consistent and manageable format.

