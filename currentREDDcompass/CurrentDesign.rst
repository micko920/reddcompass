.. _rc1_overview:

Current Version 1 Design
========================



Menus and Controls
------------------

This is the controls for updating the state of the application and performing
actions.

Register and Language Selection
```````````````````````````````
.. thumbnail:: figures/RegisterLanguageBar.png
  :width: 100px
  :align: right

If the user is not registered then the can register quickly. The language for
the application can also be set.

Application Menu
````````````````
.. thumbnail:: figures/MenuAndLogo.png
  :width: 100px
  :align: right

All general operations for the application can be performed from the contexted
based menu.


Navigation
----------

The focus of the web site is to allow the reader to find and read sections of
the MGD. The website navigation does this by using multiple controls. Each
control presents the `MGD contents` with a different information model. The
`pyramid` showing the concepts in a theme with a suggesting of order of
consideration. The Related concepts table highlighting the other concepts to
read in conjunction to a selected concept. The MGD sections providing the list
of MGD references related to a Theme/Concept.

Other non-navigation controls will have elements which will enable linking to
other content. The Navigation Controls will update to reflect the current
location in their respective information model.

Pyramid: Concepts and Stacking of content
`````````````````````````````````````````
.. thumbnail:: figures/ThemePyramid.png
  :width: 100px
  :align: right

The Pyramid content model is suggest that the lower content should be
understood and established first prior to attempting the high stack content.
This is not a strict rule as more a guide to establish a small amount of
direction on where to start. As the sophistication of the NFMS develops each
area may develop overtime and improve requiring a person to need to review the
lower content.

Related Concepts: Links and cross cutting thread of information
```````````````````````````````````````````````````````````````
.. thumbnail:: figures/RelatedConcepts.png
  :width: 100px
  :align: right

Content is not understood in isolation. Related content to keeping mind or
understand in connection is indicated to the reader by highlighting related
concepts once a concept is selected.

MGD Sections: Direct MGD content links
``````````````````````````````````````
.. thumbnail:: figures/MGDSectionsList.png
  :width: 100px
  :align: right

The MGD sections lists the content of the MGD that relates to the
Theme/Concept.  This information is presents direct external references.

This control drives the MGD content view. In general it does not 'jump' out of
the current Theme/Concept context.

Information, Content and Reference
----------------------------------

Information presented to the reader is represented as Actions, MGD content, and
general organisational information panels.

Where possible if links are provided to other content the colours and styles
are maintained for consistency. The only exception to this is the MGD content.
This is styled to represent a 'reading' experience and the only styles used
should enable clarity in reading.

Action Information Panel
````````````````````````
.. thumbnail:: figures/ActionList.png
  :width: 100px
  :align: right

.. thumbnail:: figures/ActionInfoPanel.png
  :width: 100px
  :align: right

MGD Content View
````````````````
.. thumbnail:: figures/MGDView.png
  :width: 100px
  :align: right

Footer Information
``````````````````
.. thumbnail:: figures/CopyrightFooter.png
  :width: 100px
  :align: right


Data, information, and feedback
-------------------------------

Action Response Panel
`````````````````````
.. thumbnail:: figures/ActionResponcePanel.png
  :width: 100px
  :align: right

Contact Us Panel
````````````````
.. thumbnail:: figures/ContactPanel.png
  :width: 100px
  :align: right


Terminology and Nomenclature
----------------------------




+---------+-----------------------------+-------------+
+ Meaning + Symbol                      + Description +
+=========+=============================+=============+
+         + :awesome:`fa-certificate`   +             +
+---------+-----------------------------+-------------+
+         + :awesome:`fa-search`        +             +
+---------+-----------------------------+-------------+
+         + :awesome:`fa-download`      +             +
+---------+-----------------------------+-------------+
+         + :awesome:`fa-bars`          +             +
+---------+-----------------------------+-------------+
+         + :awesome:`fa-plus-circle`   +             +
+---------+-----------------------------+-------------+
+         + :awesome:`fa-minus-circle`  +             +
+---------+-----------------------------+-------------+
+         + :awesome:`fa-book`          +             +
+---------+-----------------------------+-------------+
+         + :awesome:`fa-star`          +             +
+---------+-----------------------------+-------------+
+         + :awesome:`fa-external-link` +             +
+---------+-----------------------------+-------------+
+         + |MGD-Nav-leftDouble|        +             +
+---------+-----------------------------+-------------+
+         + |MGD-NAV-leftSingle|        +             +
+---------+-----------------------------+-------------+
+         + |MGD-Nav-rightSingle|       +             +
+---------+-----------------------------+-------------+
+         + |MGD-Nav-rightDouble|       +             +
+---------+-----------------------------+-------------+
+         + |GFOI-Logo-circle|          +             +
+---------+-----------------------------+-------------+
+         + |LanguageFlags|             +             +
+---------+-----------------------------+-------------+
+         + |PyramidNotSelected|        +             +
+---------+-----------------------------+-------------+
+         + |PyramidBorderSelected|     +             +
+---------+-----------------------------+-------------+




.. |MGD-Nav-leftDouble| thumbnail:: figures/MGD-Nav-leftDouble.png
    :width: 20px

.. |MGD-NAV-leftSingle| thumbnail:: figures/MGD-NAV-leftSingle.png
    :width: 20px

.. |MGD-Nav-rightSingle| thumbnail:: figures/MGD-Nav-rightSingle.png
    :width: 20px

.. |MGD-Nav-rightDouble| thumbnail:: figures/MGD-Nav-rightDouble.png
    :width: 20px

.. |GFOI-Logo-circle| thumbnail:: figures/GFOI-Logo-circle.png
    :width: 20px

.. |LanguageFlags| thumbnail:: figures/LanguageFlags.png
    :width: 20px

.. |PyramidNotSelected| thumbnail:: figures/PyramidNotSelected.png
    :width: 20px

.. |PyramidBorderSelected| thumbnail:: figures/PyramidBorderSelected.png
    :width: 20px




