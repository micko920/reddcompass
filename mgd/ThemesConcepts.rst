.. _rc2_mgd_themesConcepts:

Themes and Concepts
===================

.. todo:: add change log item here for the new theme `observation`



Themes
------

Institutional Arrangements (IA)
```````````````````````````````

* Methods and Tools
* Processes
* Forest Policy and Governance
* MRV Institutions

Policy and Design Decisions (PDD)
`````````````````````````````````

* Spatial and Temporal Scope
* LULC Stratification Scheme
* Approaches, Methods and Tiers
* Forest Definition
* REDD+ Activities
* Carbon Pools

Observations (OB) [*new*]
`````````````````````````
.. Note:: This has changed since version 1.
  The `Observations` Theme has been added.

* Combining [*new*]
* Selection [*new*]
* Remote
* Ground Based


Measurement and Estimation (ME) [*changed*]
```````````````````````````````````````````
.. Note:: This has been changed since version 1.
  Some concepts have been moved to the new `Observations (OB) [new]`_ theme.

* Estimation and Integration
* Uncertainty
* Activity Data
* Emission Factors
* Remote Sensing Observations [moved to observations]
* Ground Based Sensing Observations [moved to observations]


Reporting and Verification (RV)
```````````````````````````````

* Internal and External Analysis
* Reference Emission Levels
* REDD+ Reporting
* AFOLU GHG Inventory Reporting
* Non-carbon Related Reporting

Concepts
--------


.. todo:: add the concept notes

