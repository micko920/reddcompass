
.. _rc2_mgd_overview:

MGD Publication
===============

.. toctree::
  :hidden:
  :glob:

  *

This is the REDDcompass design aspects that relate to the publication
functionality for the GFOI MGD. The general tools set is referred to as
REDDcompass but incorporates the MGD publication, Actions, ToolsRepo, and
Country Needs Assessment.  These other parts of REDDcompass are explained in
other design sections.


The publication of the Methods and Guidance Document (**MGD**) is done in
multiple forms; HTML, PDF, hard copy.

It is structured using three levels of numbered headings. In addition there are
appendices, boxes, table, figures, equations, acronyms, and terms.

.. hlist::
  :columns: 3

  - **MGD Headings** |ExampleMGDHeading|
  - **MGD Box** |ExampleMGDbox|
  - **MGD Appendix** |ExampleMGDappendix|

.. |ExampleMGDHeading| thumbnail:: figures/ExampleMGDHeading.png
    :width: 100px

.. |ExampleMGDbox| thumbnail:: figures/ExampleMGDbox.png
    :width: 100px

.. |ExampleMGDappendix| thumbnail:: figures/ExampleMGDappendix.png
    :width: 100px


All of these document elements are linkable and can be referenced by other parts
of the REDDcompass system. See :ref:`rc2_mgd_content` for more.

In addition a information model called :ref:`rc2_mgd_themesConcepts` has been
created from a simple abstraction of the content. This supports navigation,
translation, indexing, and general content categorisation.













