.. _rc2_actions_overview:

Actions
=======

There are a wide range of "Things a system needs" to operate effectively. These
"things a system needs" are represented as Actions. Each action by itself
represents a "chunk" of what is needed. The actions are then used to focus the
review and identification of tangible parts of the system to be assessed.

Visually a REDDcompass action is represented as a card.

|ExampleActionCard|


.. |ExampleActionCard| thumbnail:: figures/RC-ActionCard.png
    :align: center
    :width: 400px

User State Diagram for Actions
------------------------------

The actions can be used during a review of an NFMS. Each action can be assessed
against the NFMS to understand if the current operation of the NFMS exhibits
the intent of the action.  The assessment of an action is indicated by adding
Present (P), Suitable (S), Operational (O), and Effective (E). The PSOE
categories are described in `Definitions of the PSOE Categories`_.

.. uml::
  :align: center

  @startuml
  !include stylesheet.iuml
  scale 200 width
  hide empty description

  state "Unknown State of Actions" as unkActions
  state "Subset of Actions Accessed" as someKwnActions

  [*] --> unkActions
  unkActions --> someKwnActions: action assessment
  someKwnActions --> someKwnActions : update action assessments
  someKwnActions --> [*]

  @enduml

For effective use of the actions it is not required to assess all actions. In
some cases only a subset of `interrelated actions`__ would need to be assessed
before a user could move onto creating an improvement plan and work pages.

__ rc2_actions_interrelationshipAndGroupofActions_

Definitions of the PSOE Categories
----------------------------------

Present (P):
  There is evidence that the implementation is clearly visible and is
  documented within the NFMS Documentation.
Suitable (S):
  The implementation in the NFMS is suitable based on size, nature, complexity.
Operational (O):
  There is evidence that the element the card represents is in use and an
  output is being produced.
Effective (E):
  There is evidence that the element the card represents is effectively
  achieving the desired outcome for the **whole** NFMS.


.. _rc2_actions_interrelationshipAndGroupofActions:

Interrelationship and groups of Actions
---------------------------------------
The sub set of actions to be included in an improvement plan will be based on
many factors. There are different methods of dividing up the actions while
making the choice of which actions to consider and then subsequently include
for improvement can be divided into different groups.

Recommended:
  Predefined action lists recommended by supporting agencies.

System qualities and principles:
  Cross cutting concepts such as QA/QC, record keeping, and instruction, and
  verification documentation are cross cutting issues.

Expanding Mandate:
  New operational objectives of an NFMS may require improvements to different
  aspects and parts of an NFMS.

Priority and Focus:
  Stakeholder context will influence the actions to be considered.

Review, Issue tracking, and maintenance:
  In operation quick corrections, fixes, and systematic failures can
  accumulate.  This then may require a larger concerted effort with a larger
  impact to permanently fix and improve to system.

Progress system maturity and sophistication:
  In the development of the NFMS there will be constraints put on the
  investment of time and resources.  Overtime and following success these
  restraints many be reconsidered and changed.


.. todo:: Explain the aims of action selection

  Aim:
    - It is known that "too much" change can be destructive of working and
      established functional parts of a system.
    - It is not "good" practice to bit off more than you can chew.
    - It is better to be selective in the changes you make.
    - Complete consistent change








