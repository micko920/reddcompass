.. _rc2_overview:

Version 2 Overview
==================

This is the Documentation and Design artefacts for the REDDcompass 2 system.

In general the REDDcompass system referees to all parts, MGD publication,
Actions, Tools Registry, and Country Needs Assessment.

Each section will detail the sub parts of the REDDcompass system. The links for
each section are listed on the side bar to the left. Below is a high level
overview with a brief description of what is covered.

  - :ref:`rc2_mgd_overview`:

    This section covers the detail of the MGD and the reference material.
    Included in this is the structure of the content which is broken into
    themes and key concepts.

  - :ref:`rc2_actions_overview`

    + CNA REDDcompass action cards
    + Action information and response panel

  - :ref:`rc2_toolrepo_overview`

    + Tool Search
    + Tool Criteria compare
    + Tool Profile

  - :ref:`rc2_cna_overview`

    + General information card link


Some detail and material about the REDDcompass Version 1.0 is covered in
:ref:`rc1_overview`


User Interface
--------------
The user interface is detailed in the UI section. The |UIOverview| shows the
main modes of the interface. See :ref:`rc2_ui_overview`


.. |UIOverview| thumbnail:: ui/figures/AllInformationModelsOverview.png
    :width: 100px


System Logical Block Model
--------------------------
|Logical Blocks|

The MGD content is central to the REDDcompass system. All logical blocks and
Information models are built up from this basis.

GFOI MGD Web Application Mission Statement
  The GFOI MGD Web Application enables countries to define their circumstances,
  identify methods and approaches that best meet their needs, and work through
  the development of their specific NFMS meeting MRV requirements

System design principles:
  - The MGD content is the foundation and priority of the website
  - All other features of the website should enhance the use of the MGD content

.. |Logical Blocks| thumbnail:: figures/Overview-LogicalBlocks.JPG
  :align: right
  :width: 100px

.. uml::

  @startuml
  !include stylesheet.iuml

  package "MGD Content" {
    interface "MGD Heading Number" as mgd_heading
    mgd_heading -- [Headings and Secitons]
    [Content Page View] -up-> mgd_heading
    [MGD Sections List] -up-> mgd_heading
  }

  together {
    package "Theme and Concepts" {
      [Theme and Concepts] --> mgd_heading
      [Related Theme and Concepts] --> mgd_heading
      [Front Page Theme Boxes] --> mgd_heading
    }

    package "Actions" {
      [Action Card] --> mgd_heading
      [Action Information Panel] --> mgd_heading
    }
  }

  together {
    package "CNA and NFMS" {
      [NFMS CARD] --> mgd_heading
    }

    package "Tool Repo" {
      [Tool Profile] --> mgd_heading
      [Tool Criteria] --> mgd_heading
    }
  }
  @enduml




Information Models
------------------

The content of the MGD can be represented and linked by using different
information models. The simplest form being the heading hierarchy of the
document itself. Each information model allows a different order and priority
of relevant MGD content.

The information models are listed below:

  - MGD contents hierarchy
      The MGD contents hierarchy is important in this scientific context. It is
      standard amongst scientific publications to reference other material. To
      aid this the document has a clear numbered hierarchy.
      This hierarchy is linked to the dated and versioned release of the
      MGD.

  - Theme and Concepts
      The Theme and Concept information model groups content in a similar way
      to the heading hierarchy of the document. It is more generic with a focus
      on aspects and terminology of the general design and operation of NFMS
      and the application of MRV principles.
      The navigation is enhanced by using distinguishing colours and pyramid
      model for implying a soft level of order to the concepts.

  - Actions
      The actions are a tasked based categorisation. This allows prioritisation
      of 'things to do' based on levels of current sophistication and aims of
      improvement. For example categories of QA/QC and Record Keeping can apply
      to multiple parts of a NFMS and may be tailored so that the
      implementation is appropriate for the cost, complexity and resources
      available.

  - NFMS
      The National Forest Monitoring System is an information model based on
      the operational arrangement of a system. Categorisation of the MGD
      content is focused around the parts of a NFMS, such as Documentation,
      Processes, Operational Roles, Data, and Reports.

Linking and cross cutting
-------------------------

.. todo:: Detail how the links between logic blocks will be done




