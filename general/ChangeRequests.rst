.. rc2_changerequests:

Change Requests
===============


Filter result as table
----------------------

.. needtable::
  :types: sgtn;bug
  :columns: id; tags; title
  :style: table


Suggested REDDcompass Website Improvements - Prepared by Sarai Piazza
---------------------------------------------------------------------
The REDDcompass website is a useful, user friendly website. It provides one
consolidated location for resources, tools, and information for decision
support relative to the complicated MRV process while providing information in
a logical step by step manner.  The website does a great job of presenting a
network of linkages in the complicated MRV process into an understandable
format. The MRV profile component provides a place to progressively work
through themes, concepts, and actions and store information.

List of suggested website improvements based on my experience at the ROC and
Guatemala trainings in the fall of 2017

.. sgtn:: MGD search in other languages
  :status: open
  :tags: silvacarbon; v2

    1) Search MGD is a powerful tool currently linked to the English version of
    the document. Is there any way the search function could be
    available/linked to the Spanish and French versions of the MGD?

.. sgtn:: Simplify registration process
  :status: open
  :links: REG_IMPROVEMENT
  :tags: silvacarbon; v2

    2) Can the website registration process be simplified? Is it possible to
    combine the email verification and temporary password into one email
    instead of 2 emails? Provide a clickable link in the email that will verify
    the email address and then the user can simply enter the temporary
    password. That would eliminate the need to reenter the long email
    verification code and wait on two individual emails. In both trainings I
    have attended, internet connectivity was unreliable.

.. sgtn:: Website parts able to operate offline
  :status: open
  :tags: silvacarbon; v2; cnaCards

    3) Users can download many of the tools, resources, and the MGD to work
    offline. Is it possible to have other components of the website set to be
    downloadable? Such that large components of the website could work offline?
    With unreliable internet connectivity, it would be great if users could
    somehow have access to the themes, concept pyramids, and action worksheet
    information while offline. However, I understand that it may not be
    possible and the size of the downloads required might be prohibitive with
    slow, intermittent internet connectivity. If this was accomplished, an
    “upload” MRV profile feature would have to be added. So that once users
    were ready to submit their MRV profile back into the website, they could.

.. sgtn:: Change the term 'name' used for profile to be 'MRV Profile Name'
  :status: open
  :tags: silvacarbon; v2

    4) When creating a MRV profile the term “name” is a bit confusing because
    it isn’t clear if it is supposed to be the person’s name who is creating
    the profile. Could it be changed to “MRV Profile Name”


.. sgtn:: More documentation or clarification about 'owner role'
  :status: open
  :tags: silvacarbon; v2

    5) When creating a MRV profile a dropdown list for owner role or an
    “information” button explaining what is intended by owner role might be
    helpful.

.. bug:: Action response dropdown list are always in English
  :status: open
  :tags: silvacarbon; v2

    6) Within the MRV profiles, the competency, training, and tag dropdown
    lists are in English no matter what language the website is set to. Can
    they be translated?

.. bug:: Cannot remove last entry or clear an entry on action response
  :status: open
  :tags: silvacarbon; v2

    7) Add a blank or reset button to tags, competencies, and action state. I
    believe that currently if the user selects an item from the drop down list,
    they are not able to go back and “reset” the selection to no entry.

.. sgtn:: Add extra specific information capture for training request
  :status: open
  :tags: silvacarbon; v2

    8) Add a text box to enter specific training needs within competency.
    Because some of the actions involve many steps, the training needs may
    vary. Currently the user can enter specific training information in
    “Response” however I imagine a great deal of other types of information is
    also being entered into the response textbox.  If there was a place to
    enter specific training needs within competency, those data would be easier
    to mine on the backend for GFOI and then communicated to the larger
    capacity building community.

.. sgtn:: More documentation about meaning of fields and values in Action
          response
  :status: open
  :tags: silvacarbon; v2

    9) Descriptions for all tags, competencies, etc. This might be accomplished
    with an “Information” button. When clicked a pop up document would appear
    describing each element in detail.  The tags options, and ability to choose
    more than one tag, are a bit confusing without further description of what
    was intended by each. Carly provided more context in an email to Sylvia
    while we were in Guatemala and I was able to include the information during
    the presentation, but I think for clarity on the website, more explanation
    is needed for the general user.

.. sgtn:: Tag to indicate that a profile is official or master
  :status: open
  :tags: silvacarbon; v2

    10) Indication of whether the MRV profile is official or not. Such that if
    work is being divided up within a country’s agencies there would be a way
    to differentiate between the working MRV profiles and the
    official/consolidated MRV profile. This would also help with what training
    needs a country at large is interested in pursuing.

.. sgtn:: Function to merge MRV profiles
  :status: open
  :links: PROFILE_MERGE
  :tags: silvacarbon; v2

    11) Ability to share, consolidate, or merge information stored in multiple
    MRV profiles.

.. sgtn:: Detail and documentation on used and publication of response
          information
  :status: open
  :tags: silvacarbon; v2

    12) Provide a disclosure that gives information regarding who has access to
    MRV profile info on the backend. If any content, in addition to website use
    statistics from google analytics, are being used then users should be made
    aware upfront.  Are the contents being reviewed by GFOI (or other entities)
    and reported out to other stakeholders? How often are the profiles mined
    for training needs? How does GFOI communicate training needs to the
    capacity building community? How does the user know when to expect
    feedback?


Work planning 2018
------------------

.. sgtn:: Improvements to registration process (bulk registration)
  :status: open
  :id: REG_IMPROVEMENT
  :tags: v2; high

  GFOI audiences are struggling with the standard method for registration.
  Changes to registration process should take into consideration both the user
  and GFOI stakeholder engagement processes.

.. sgtn:: Profile merge
  :status: open
  :id: PROFILE_MERGE
  :tags: v2; high

  Developing roadmaps is a collaborative effort. Collaboration between users in
  a group training environment or between agencies in national situations
  cannot combine profiles to complete the roadmaps without cut and paste
  through external tools.

.. sgtn:: Enhanced export and printing of profile
  :status: open
  :tags: v2; high; reporting;

  Control P and table selection is hard for users to print and capture data
  into other tools for reporting purposes. Unfunded small changes have been
  implemented to overcome immediate challenges has proven useful however this
  is not a complete solution and a more sophisticated export/printing function
  is required.

.. sgtn:: Improvement and extended annotation of actions and their different
          types of relationships
  :status: open
  :tags: v2; medium

  Both trainers and students can struggle to understand the implications of the
  actions across related concepts.

.. sgtn:: Improved layout and presentation of related training material and
          tools referenced to MGD content
  :status: open
  :tags: v2; medium

  Updates should focus on improvements that keep the platform relevant as the
  user base becomes more sophisticated. As more material is produced
  opportunities are missed in interacting with the Action interface. Some
  Actions could have specialised tools which could benefit from tighter
  integration with REDDcompass.

.. sgtn:: Edit Profile Name
  :status: open
  :id: EDIT_PROFILE_NAME
  :tags: v2; low

  Users are required to make a new profile and start again if they want to
  change the name. The personal ability to edit the user profile will give
  users more control over their interaction with REDDcompass.


Concept note on enhancing capacity building for REDD+ MRV using REDDcompass
---------------------------------------------------------------------------
This is a summary extraction from the planning and notes between Carly Green
and Thomas Harvey over 2018 and early 2019 in relation to GFOI phase II.

Only the REDDcompass items have been recorded here.

Activities
``````````
1) Targeted modifications to the REDDcompass;

.. sgtn:: MRV Maturity Model
  :status: open
  :tags: v2; reporting;

  a. Structural modifications to make REDDcompass enhancing its applicability
  to countries at different levels of REDD+ readiness, including early, medium
  and advanced levels of implementation.

.. sgtn:: Integration of extended and additional resources into REDDcompass
  :status: open
  :tags: v2;

  b. Embed capacity building materials, such as  on-line lectures, videos,
  presentations and examples.

.. sgtn:: Action Panel Integration with other tools
  :status: open
  :tags: v2;

  c. Embed and directly link technical tools (e.g. SEPAL, Open Foris, and
  Collect Earth Online etc) to enable users to implement technical tasks whilst
  working through the tool, including links to available geospatial resources
  (google earth engine).

.. sgtn:: Country Status and Summary Information
  :status: open
  :tags: v2; reporting;

  d. Improve and extend country level reporting features of REDDcompass to
  enable clearer communication between users and GFOI.

2) Application and implementation of REDDcompass to assess MRV gaps and needs
for key REDD+ countries, to track progress, and to map donor interventions;

.. sgtn:: MRV Gaps and Need Assessment
  :status: open
  :tags: v2;

  a. Application of REDDcompass for rapid assessment of MRV gaps and needs.

.. sgtn:: Country assessment with MRV Maturity Model
  :status: open
  :tags: v2; reporting;

  b. Implementation of REDDcompass for tracking MRV progress with key
  indicators as countries move through REDD+ phases.

.. sgtn:: MRV progress Dashboard
  :status: open
  :tags: v2; reporting;

  c. Development of REDDcompass dashboards for reporting and tracking MRV
  progress.

.. sgtn:: Stakeholder and Responsibility Mapping
  :status: open
  :tags: v2;

  d. Development of REDDcompass module for mapping donor interventions and
  progress to improve donor coordination.



Technical System and Platform Improvements 2018
-----------------------------------------------


.. sgtn:: Upgrade sub systems to latest major release
  :status: open
  :tags: v2;

  Upgrade Liferay version, Bootstrap libraries, and javascript libraries. There
  have been new major version releases of the systems REDDcompass is built on.
  These releases will include security fixes, user interface enhancements and
  general bug fixes. Updating regularly and keeping in sync with major version
  releases ensures the REDDcompass code does not become obsolete or too old to
  patch when critical failures are found in support systems.

.. sgtn:: Merge Profiles into single master version
  :links: PROFILE_MERGE
  :status: open
  :tags: v2;

  A profile merge feature will help groups working together where each
  individual works on an action sub set to then merge each other’s work into a
  final complete profile.

.. sgtn:: Bulk registration
  :links: REG_IMPROVEMENT
  :status: open
  :tags: v2;

  The registration process is a key problem for many using the system and needs
  improvement. In addition to this a bulk registration system for workshops
  will allow a workshop registration list to be used to bulk add registrants
  prior to an event.

.. sgtn:: Display improvements for action panel and MGD content and linkages
  :status: open
  :tags: v2;

  Improvement and extended annotation of action relationships, better layout
  and presentation of related training material and tools and referenced to MGD
  content.

.. sgtn:: Export Data feature
  :status: open
  :tags: v2; reporting;

  Enhanced export and printing of a profile will help users integrate with
  other formats and publications

.. sgtn:: New GDPR rules and changes
  :status: open
  :tags: v2; reporting;


Work Planning 2019
------------------

.. sgtn::	Update logos
  :status: open
  :tags: v2;

.. sgtn::	Restructure based on MGD review and update
  :status: open
  :tags: v2;


.. sgtn::	Update actions based on CNA feedback and data
  :status: open
  :tags: v2;

.. sgtn:: Data component meeting and operations
  :status: open
  :tags: v2;

  RoT
    - survey information
    - CALM assessments

.. sgtn:: CNA and NFMS systems view concept
  :status: open
  :tags: v2;

.. sgtn:: Tailorable and predefined subsets of action lists
  :id: ACTION_SUBSETS
  :status: open
  :tags: v2;

  The actions may not all be relevant at a particular time or for a particular
  country. Allow the user to select a sub set list of actions. This will then
  filter/hide all actions not included in the list from all screens and
  reports. This will allow the user to focus on this subset of actions.

.. sgtn:: Functionality to share a MRV profile with another user
  :status: open
  :tags: v2;


.. sgtn:: Coaches and implementation partners are creating spreadsheets based
          on the REDDcompass actions outside of REDDcompass
  :status: open
  :links: ACTION_SUBSETS;
  :tags: gfoi capacity building; v2; reporting;

  The reasons for this:
  - Can not create a subset of actions
  - Bespoke action information needs to be captured
  - Need to report and collate information in particular way

