# -*- coding: utf-8 -*-
#
# Configuration file for the Sphinx documentation builder.
#
# This file only contains a selection of the most common options. For a full
# list see the documentation:
# http://www.sphinx-doc.org/en/master/config

# -- Path setup --------------------------------------------------------------

# If extensions (or modules to document with autodoc) are in another directory,
# add these directories to sys.path here. If the directory is relative to the
# documentation root, use os.path.abspath to make it absolute, like shown here.
#
# import os
# import sys
# sys.path.insert(0, os.path.abspath('.'))
import ablog
import sys
import os

sys.path.append(os.path.abspath("./source/_ext"))
sys.path.append(os.path.abspath("./_ext"))
sys.path.append(os.path.abspath("."))

# -- Project information -----------------------------------------------------

project = 'GFOI REDDcompass 2'
copyright = '2019, Michael Green'
author = 'Michael Green'

# The short X.Y version
version = '2.0.1'
# The full version, including alpha/beta/rc tags
release = '2.0 design'


# -- General configuration ---------------------------------------------------

# If your documentation needs a minimal Sphinx version, state it here.
#
# needs_sphinx = '1.0'

# Add any Sphinx extension module names here, as strings. They can be
# extensions coming with Sphinx (named 'sphinx.ext.*') or your custom
# ones.
extensions = [
    'sphinx.ext.intersphinx',
    'sphinx.ext.todo',
    'sphinx.ext.coverage',
    'sphinx.ext.mathjax',
    'sphinx.ext.ifconfig',
    'sphinx.ext.autodoc',
    'sphinxcontrib.plantuml',
    'sphinxcontrib_awesome',
#    'sphinxcontrib.jinjadomain',
    'sphinxcontrib.needs',
    'sphinxcontrib.spelling',
    'sphinxcontrib.images',
    'sphinx_rtd_theme',
    'micko920_embed',
    'ablog'
]

# Add any paths that contain templates here, relative to this directory.
templates_path = ['_templates']
templates_path.append(ablog.get_html_templates_path())

# The suffix(es) of source filenames.
# You can specify multiple suffix as a list of string:
#
# source_suffix = ['.rst', '.md']
source_suffix = '.rst'

# The master toctree document.
master_doc = 'index'

# The language for content autogenerated by Sphinx. Refer to documentation
# for a list of supported languages.
#
# This is also used if you do content translation via gettext catalogs.
# Usually you set "language" from the command line for these cases.
language = None

# List of patterns, relative to source directory, that match files and
# directories to ignore when looking for source files.
# This pattern also affects html_static_path and html_extra_path.
exclude_patterns = []

# The name of the Pygments (syntax highlighting) style to use.
pygments_style = None


# -- Options for HTML output -------------------------------------------------

# The theme to use for HTML and HTML Help pages.  See the documentation for
# a list of builtin themes.
#
html_theme = 'sphinx_rtd_theme'
#html_theme = 'alabaster'

# Theme options are theme-specific and customize the look and feel of a theme
# further.  For a list of options available for each theme, see the
# documentation.
#
html_theme_options = {}
# html_theme_options = {
#     'page_width':"100%",
#     'sidebar_width':"20%"
# }
# Add any paths that contain custom static files (such as style sheets) here,
# relative to this directory. They are copied after the builtin static files,
# so a file named "default.css" will overwrite the builtin "default.css".
html_static_path = ['_static']

# These paths are either relative to html_static_path
# or fully qualified paths (eg. https://...)
html_css_files = [
    'css/custom.css',
]

# Custom sidebar templates, must be a dictionary that maps document names
# to template names.
#
# The default sidebars (for documents that don't match any pattern) are
# defined by theme itself.  Builtin themes are using these templates by
# default: ``['localtoc.html', 'relations.html', 'sourcelink.html',
# 'searchbox.html']``.
#
# html_sidebars = {}
html_sidebars = { 'blogItems/**' : [ 'postcard.html', 'recentposts.html',
          'tagcloud.html', 'categories.html',
          'archives.html' ]
}


# -- Options for HTMLHelp output ---------------------------------------------

# Output file base name for HTML help builder.
htmlhelp_basename = 'reddcompass'


# -- Options for LaTeX output ------------------------------------------------

latex_elements = {
    # The paper size ('letterpaper' or 'a4paper').
    #
    # 'papersize': 'letterpaper',

    # The font size ('10pt', '11pt' or '12pt').
    #
    # 'pointsize': '10pt',

    # Additional stuff for the LaTeX preamble.
    #
    # 'preamble': '',

    # Latex figure (float) alignment
    #
    # 'figure_align': 'htbp',
}

# Grouping the document tree into LaTeX files. List of tuples
# (source start file, target name, title,
#  author, documentclass [howto, manual, or own class]).
latex_documents = [
    (master_doc, 'reddcompass.tex', 'GFOI REDDcompass 2',
     'Michael Green', 'manual'),
]


# -- Options for manual page output ------------------------------------------

# One entry per manual page. List of tuples
# (source start file, name, description, authors, manual section).
man_pages = [
    (master_doc, 'reddcompass', 'GFOI REDDcompass 2',
     [author], 1)
]


# -- Options for Texinfo output ----------------------------------------------

# Grouping the document tree into Texinfo files. List of tuples
# (source start file, target name, title, author,
#  dir menu entry, description, category)
texinfo_documents = [
    (master_doc, 'reddcompass', 'GFOI REDDcompass 2',
     author, 'reddcompass', 'GFOI REDDcompass 2.',
     'Miscellaneous'),
]


# -- Options for Epub output -------------------------------------------------

# Bibliographic Dublin Core info.
epub_title = project

# The unique identifier of the text. This can be a ISBN number
# or the project homepage.
#
# epub_identifier = ''

# A unique identification for the text.
#
# epub_uid = ''

# A list of files that should not be packed into the epub file.
epub_exclude_files = ['search.html']


# -- Extension configuration -------------------------------------------------

# -- Options for intersphinx extension ---------------------------------------

# Example configuration for intersphinx: refer to the Python standard library.
intersphinx_mapping = {'rst': ('http://www.sphinx-doc.org/en/master/', None)}

# -- Options for todo extension ----------------------------------------------

# If true, `todo` and `todoList` produce output, else they produce nothing.
todo_include_todos = True

# -- Options for ablog extension ----------------------------------------------

blog_path = 'source/blogItems'
blog_title = 'Activity Blog'
#blog_baseurl = ''

# -- Spelling settings -------------------------------------------------------

spelling_lang='en_GB'
tokenizer_lang='en_GB'
spelling_show_suggestions=False

spelling_word_list_filename = ["../en.utf-8.add"]

# -- Plantuml executable -----------------------------------------------------
#plantuml = 'plantuml'

# -- Sphinxcontrib Needs -----------------------------------------------------
needs_types = [
  dict(directive="req", title="Requirement", prefix="R_", color="#BFD8D2", style="node"),
  dict(directive="sgtn", title="Suggestion", prefix="C_", color="#4db6b0", style="node"),
  dict(directive="bug", title="Issue", prefix="B_", color="#ff0000", style="node"),
  dict(directive="spec", title="Specification", prefix="S_", color="#FEDCD2", style="node"),
  dict(directive="impl", title="Implementation", prefix="I_", color="#DF744A", style="node"),
  dict(directive="test", title="Test Case", prefix="T_", color="#DCB239", style="node"),
  # Kept for backwards compatibility
  dict(directive="need", title="Need", prefix="N_", color="#9856a5", style="node")
]

# conf.py
#needs_table_style = "datatables"
