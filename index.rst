.. GFOI REDDcompass 2 documentation master file, created by
   sphinx-quickstart on Wed Jul 24 14:00:43 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to GFOI REDDcompass 2's design documentation!
=====================================================
This is the design artefacts for REDDcompass 2.


In general the REDDcompass system referees to all parts, MGD publication,
Actions, Tools Registry, and Country Needs Assessment.

Each section below will detail the sub parts of the REDDcompass system.




.. toctree::
   :maxdepth: 1
   :includehidden:
   :caption: Contents:
   :glob:

   readme.rst
   mgd/MGDContentOverview.rst
   rcActions/RCActionsOverview.rst

.. toctree::
   :maxdepth: 2
   :includehidden:
   :caption: User Interface
   :glob:

   ui/UIOverview.rst
   ui/UIDesign.rst

.. toctree::
   :maxdepth: 2
   :includehidden:
   :caption: Tool Registry
   :glob:

   toolsrepo/ToolsRepoOverview.rst
   toolsrepo/ToolsRepoPrototype.rst
   toolsrepo/ToolsRepoRequirements.rst
   toolsrepo/ToolsRepoDataFields.rst


.. toctree::
   :maxdepth: 1
   :includehidden:
   :caption: Country Needs Assessment
   :glob:

   cna/CNAOverview.rst


.. toctree::
   :maxdepth: 1
   :includehidden:
   :caption: Notes
   :glob:

   currentREDDcompass/CurrentDesign.rst
   general/TodoList.rst
   general/ChangeRequests.rst
   general/ChangeLog.rst
   general/Requirements.rst



Indices and tables
==================

* :ref:`search`
